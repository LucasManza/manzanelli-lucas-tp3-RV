﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Advertisements;

public class WinMessage : MonoBehaviour
{

    public int Seconds;
    private void Awake()
    {
        gameObject.SetActive(false);
    }

    public void ShowWinMessage()
    {
        Debug.LogError("WINNING!");

        gameObject.SetActive(true);
        StartCoroutine(WaitForSeconds());
    }

    private IEnumerator WaitForSeconds()
    {
        yield return new WaitForSeconds(Seconds);
        gameObject.SetActive(false);
        yield return null;
    }

}
