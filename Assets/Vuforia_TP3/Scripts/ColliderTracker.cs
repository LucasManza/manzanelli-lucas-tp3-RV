﻿using UnityEngine;
using Vuforia;

public class ColliderTracker : MonoBehaviour, ITrackableEventHandler
{
    public TrackableBehaviour TrackableBehaviour;
    public Number Number;
    private BoxCollider _boxCollider;

    void Start()
    {
        TrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (TrackableBehaviour)
            TrackableBehaviour.RegisterTrackableEventHandler(this);
//        CheckBoxCollider();
    }

    private void CheckBoxCollider()
    {
        if (Number.GetComponent<BoxCollider>() == null) 
        {
            Debug.LogWarning("NUMBER HASN'T BOXCOLLIDER");
            Number.gameObject.AddComponent(typeof(BoxCollider));
            Number.GetComponent<BoxCollider>().size = new Vector3(0.1f, 0.1f, 0.1f);
        }

        _boxCollider = Number.GetComponent<BoxCollider>();
    }

    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (_boxCollider == null) return;
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            // Enable BoxCollider when target is found
            _boxCollider.enabled = true;
        }
        else
        {
            // Disable BoxCollider when target is lost
            _boxCollider.enabled = false;
        }

    }
    
}