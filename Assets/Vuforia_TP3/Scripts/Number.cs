﻿using System.ComponentModel.Design.Serialization;
using System.Globalization;
using Unity.Cyberconian.Oclanay.BasicSevenSegmentDisplay;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Number : MonoBehaviour
{
    public BasicSevenSegmentDisplay DisplayTen;
    public BasicSevenSegmentDisplay DisplayUnit;

    private BoxCollider _boxCollider;

    private void Awake()
    {
        CheckBoxCollider();
    }
    
    void Start()
    {
        RandomNumber();
   
    }

    public int GetNumber()
    {
        return DisplayTen.GetNumber() * 10 + DisplayUnit.GetNumber();
    }

    public void RandomNumber()
    {
        SetNumber(Random.Range(0, 10), Random.Range(0, 10));
    }

    private void SetNumber(int unit, int ten)
    {
        SetUnit(unit);
        SetTen(ten);
    }

    private void SetTen(int ten)
    {
        DisplayTen.SetData(ten);
    }

    private void SetUnit(int unit)
    {
        DisplayUnit.SetData(unit);
    }
    
    private void CheckBoxCollider()
    {
        if (gameObject.GetComponent<BoxCollider>() != null) return;
        gameObject.AddComponent(typeof(BoxCollider));
        _boxCollider = gameObject.GetComponent<BoxCollider>();
        _boxCollider.isTrigger = true;
        _boxCollider.size = new Vector3(0.1f, 0.1f, 0.1f);
        
            

    }

    public void ChangeUnit(bool increaseBool)
    {
        var control = DisplayUnit.GetNumber();
        if (increaseBool && control < 9)
            SetUnit(++control);
        else if (!increaseBool && control > 0)
            SetUnit(--control);
    }

    public void ChangeTen(bool increaseBool)
    {
        var control = DisplayTen.GetNumber();
        if (increaseBool && control < 9)
            SetTen(++control);
        else if (!increaseBool && control > 0)
            SetTen(--control);
    }
}