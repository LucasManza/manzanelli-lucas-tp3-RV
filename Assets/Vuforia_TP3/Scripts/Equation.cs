﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Vuforia_TP3.Scripts
{
    public enum ColliderEnum
    {
        ColliderK1,
        ColliderK2,
        ColliderQ1,
        ColliderQ2,
        ColliderNoMatch
    }

    public class Equation : MonoBehaviour
    {
        private char _operandChar;

        public Number Result; 
        public Number NroK;
        public Number NroQ;
        private BoxCollider _bColK;
        private BoxCollider _bColQ;
        public Collider Collider1;
        public Collider Collider2;

        public WinMessage WinMessage;

        public Sprite[] Operands;

        public SpriteRenderer Operand;


        // Use this for initialization
        void Start()
        {
            RandomEquation();
            _bColK = NroK.GetComponent<BoxCollider>();
            _bColQ = NroQ.GetComponent<BoxCollider>();

        }

        public void RandomEquation()
        {
            int i = Random.Range(0, 4);
            Operand.sprite = Operands[i];
            
            switch (i)
            {
                case 0:
                    _operandChar = '+';
                    break;
                case 1:
                    _operandChar = '-';
                    break;
                case 2:
                    _operandChar = '*';
                    break;
                case 3:
                    _operandChar = '/';
                    break;
            }
            Debug.LogWarning(""+_operandChar);
            Result.RandomNumber();
        }

        public void CheckEquation()
        {
            var col1 = CheckCollider1();
            var col2 = CheckCollider2();
            
            if (col1 == ColliderEnum.ColliderK1 && col2 == ColliderEnum.ColliderQ2)
                CheckCalculation(NroQ.GetNumber(), NroK.GetNumber(), _operandChar);
            else if (col1 == ColliderEnum.ColliderK2 && col2 == ColliderEnum.ColliderQ2)
                CheckCalculation(NroK.GetNumber(), NroQ.GetNumber(), _operandChar);
        }
 
        private void CheckCalculation(int nro1, int nro2, char operandChar)
        {
             
            var result = 100;
            switch (operandChar)
            {
                case '+':
                    result = nro1 + nro2;
                    break;
                case '-':
                    result = nro1 - nro2;
                    break;
                case '*':
                    result = nro1 * nro2;
                    break;
                case '/':
                    result = nro1 / nro2;
                    break;
                default: 
                    result = 100;
                    break;
            }
            Debug.LogWarning("Nro1: "+ nro1 + " Nro2: "+ nro2 +" Result: " + Result.GetNumber()
                             + " Operand: "+operandChar
                             + " Estimated" + result);
            if (result != Result.GetNumber()) return;
            WinMessage.ShowWinMessage();
        }

        

        private ColliderEnum CheckCollider1()
        {
            if (_bColK.enabled && Collider1.bounds.Intersects(_bColK.bounds))
            {
//                Debug.LogWarning("K in Collider1");
                return ColliderEnum.ColliderK1;
            }

            if (!_bColQ.enabled || !Collider1.bounds.Intersects(_bColQ.bounds)) return ColliderEnum.ColliderNoMatch;
//            Debug.LogWarning("Q in Collider1");
            return ColliderEnum.ColliderQ1;

        }

        private ColliderEnum CheckCollider2()
        {
            if (_bColK.enabled && Collider2.bounds.Intersects(_bColK.bounds))
            {
//                Debug.LogWarning("K in Collider2");
                return ColliderEnum.ColliderK2;
            }

            if (!_bColQ.enabled || !Collider2.bounds.Intersects(_bColQ.bounds)) return ColliderEnum.ColliderNoMatch;
//            Debug.LogWarning("Q in Collider2 ");
            return ColliderEnum.ColliderQ2;
        }
    }
}